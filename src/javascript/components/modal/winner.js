import { showModal } from "./modal";

export function showWinnerModal(fighter) {
  const winnerInfo = {
    title: 'The WINNER is...!',
    bodyElement: fighter.name
}
    showModal(winnerInfo);
}
